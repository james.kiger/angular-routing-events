import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crisis-center',
  templateUrl: './crisis-center.component.html',
  styleUrls: ['./crisis-center.component.css']
})
export class CrisisCenterComponent {
  constructor(private router: Router) {
        this.router.events.subscribe((event) => {
          console.log('Higher Level Component');
          console.log(event);
        });
  }
}
